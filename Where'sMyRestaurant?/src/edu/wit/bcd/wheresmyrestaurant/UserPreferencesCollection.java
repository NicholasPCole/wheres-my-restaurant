package edu.wit.bcd.wheresmyrestaurant;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class UserPreferencesCollection extends Activity {
    Spinner cuisine = null, typeRestaurant = null, ambiance = null,
            rating = null, budget = null;
    EditText locationZip = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_preferences_collection);

        cuisine = (Spinner) findViewById(R.id.type_cuisine);
        ArrayAdapter<CharSequence> theCuisines = ArrayAdapter
                .createFromResource(this, R.array.cuisine_types,
                        android.R.layout.simple_spinner_item);
        theCuisines
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cuisine.setAdapter(theCuisines);

        typeRestaurant = (Spinner) findViewById(R.id.type_restaurant);
        ArrayAdapter<CharSequence> theTypesRestaurants = ArrayAdapter
                .createFromResource(this, R.array.restaurant_types,
                        android.R.layout.simple_spinner_item);
        theTypesRestaurants
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeRestaurant.setAdapter(theTypesRestaurants);

        ambiance = (Spinner) findViewById(R.id.ambiance);
        ArrayAdapter<CharSequence> theAmbianceOptions = ArrayAdapter
                .createFromResource(this, R.array.ambiance_types,
                        android.R.layout.simple_spinner_item);
        theAmbianceOptions
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ambiance.setAdapter(theAmbianceOptions);

        rating = (Spinner) findViewById(R.id.rating);
        ArrayAdapter<CharSequence> theRatings = ArrayAdapter
                .createFromResource(this, R.array.rating_options,
                        android.R.layout.simple_spinner_item);
        theRatings
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rating.setAdapter(theRatings);

        budget = (Spinner) findViewById(R.id.budget);
        ArrayAdapter<CharSequence> theBudgets = ArrayAdapter
                .createFromResource(this, R.array.budget_options,
                        android.R.layout.simple_spinner_item);
        theBudgets
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        budget.setAdapter(theBudgets);

        locationZip = (EditText) findViewById(R.id.location_zip);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_preferences_collection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.reset_form:
                resetForm();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void submitForm(View view) {
        // First validate free-form preferences.

        String enteredLocationZip = locationZip.getText().toString();
        if (enteredLocationZip.length() > 0 && enteredLocationZip.length() != 5) {
            Toast.makeText(
                    getApplicationContext(),
                    "Error: Location (zip code) must be exactly five digits. "
                            + "Search query not submitted.",
                    Toast.LENGTH_LONG).show();
            return;
        }

        // Assuming that all input has been validated at this point, start with
        // the base SELECT query.

        String userGeneratedQuery = "SELECT * FROM wmr.all_restaurants_details";
        int conditions = 0;

        // Then check each of the preferences to see if an option has been
        // selected and should be appended to the SELECT statement as a WHERE or
        // AND clause.

        if (cuisine.getSelectedItemPosition() > 0) {
            String selectedCuisine = cuisine.getItemAtPosition(
                    cuisine.getSelectedItemPosition()).toString();

            if (conditions == 0) {
                userGeneratedQuery += " WHERE";
            } else {
                userGeneratedQuery += " AND";
            }

            userGeneratedQuery += " cuisine='" + selectedCuisine + "'";
            conditions++;
        }

        if (typeRestaurant.getSelectedItemPosition() > 0) {
            String selectedTypeRestaurant = typeRestaurant.getItemAtPosition(
                    typeRestaurant.getSelectedItemPosition()).toString();

            if (conditions == 0) {
                userGeneratedQuery += " WHERE";
            } else {
                userGeneratedQuery += " AND";
            }

            userGeneratedQuery += " type='" + selectedTypeRestaurant + "'";
            conditions++;
        }

        if (ambiance.getSelectedItemPosition() > 0) {
            String selectedAmbiance = ambiance.getItemAtPosition(
                    ambiance.getSelectedItemPosition()).toString();

            if (conditions == 0) {
                userGeneratedQuery += " WHERE";
            } else {
                userGeneratedQuery += " AND";
            }

            userGeneratedQuery += " ambiance='" + selectedAmbiance + "'";
            conditions++;
        }

        if (rating.getSelectedItemPosition() > 0) {
            int selectedRating = rating.getSelectedItemPosition();

            if (conditions == 0) {
                userGeneratedQuery += " WHERE";
            } else {
                userGeneratedQuery += " AND";
            }

            userGeneratedQuery += " rating='" + selectedRating + "'";
            conditions++;
        }

        if (budget.getSelectedItemPosition() > 0) {
            int selectedBudget = budget.getSelectedItemPosition();

            if (conditions == 0) {
                userGeneratedQuery += " WHERE";
            } else {
                userGeneratedQuery += " AND";
            }

            userGeneratedQuery += " budget='" + selectedBudget + "'";
            conditions++;
        }

        if (enteredLocationZip.length() == 5) {
            if (conditions == 0) {
                userGeneratedQuery += " WHERE";
            } else {
                userGeneratedQuery += " AND";
            }

            userGeneratedQuery += " location='" + enteredLocationZip + "'";
            conditions++;
        }

        userGeneratedQuery += ";";

        if (conditions == 0) {
            Toast.makeText(getApplicationContext(),
                    "Please select at least one criteria before searching.",
                    Toast.LENGTH_LONG).show();
        } else {
            new DatabaseSearchTask(this).execute(userGeneratedQuery);
        }
    }

    private void resetForm() {
        cuisine.setSelection(0);
        typeRestaurant.setSelection(0);
        ambiance.setSelection(0);
        rating.setSelection(0);
        budget.setSelection(0);
        locationZip.setText("");

        locationZip.requestFocus();
    }
}
