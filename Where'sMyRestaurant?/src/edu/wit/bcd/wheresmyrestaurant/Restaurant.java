package edu.wit.bcd.wheresmyrestaurant;

import android.os.Parcel;
import android.os.Parcelable;

public class Restaurant implements Parcelable {
    private int id = 0;
    private String name = "";
    private String cuisine = "";
    private String type = "";
    private String ambiance = "";
    private int rating = 0;
    private int budget = 0;
    private String locationZip = "";
    private String menu = "";

    public static final Parcelable.Creator<Restaurant> CREATOR = new Parcelable.Creator<Restaurant>() {
        @Override
        public Restaurant createFromParcel(Parcel source) {
            return new Restaurant(source);
        }

        @Override
        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };

    public Restaurant(Parcel source) {
        this.id = source.readInt();
        this.name = source.readString();
        this.cuisine = source.readString();
        this.type = source.readString();
        this.ambiance = source.readString();
        this.rating = source.readInt();
        this.budget = source.readInt();
        this.locationZip = source.readString();
        this.menu = source.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getAmbiance() {
        return ambiance;
    }

    public int getBudget() {
        return budget;
    }

    public String getCuisine() {
        return cuisine;
    }

    public int getId() {
        return id;
    }

    public String getLocationZip() {
        return locationZip;
    }

    public String getMenu() {
        return menu;
    }

    public String getName() {
        return name;
    }

    public int getRating() {
        return rating;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public void writeToParcel(Parcel destination, int flags) {
        destination.writeInt(this.id);
        destination.writeString(this.name);
        destination.writeString(this.cuisine);
        destination.writeString(this.type);
        destination.writeString(this.ambiance);
        destination.writeInt(this.rating);
        destination.writeInt(this.budget);
        destination.writeString(locationZip);
        destination.writeString(this.menu);
    }
}