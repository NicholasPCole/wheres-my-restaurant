package edu.wit.bcd.wheresmyrestaurant;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A fragment representing a single Restaurant detail screen. This fragment is
 * either contained in a {@link RestaurantListActivity} in two-pane mode (on
 * tablets) or a {@link RestaurantDetailActivity} on handsets.
 */
public class RestaurantDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private Restaurant mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RestaurantDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = DatabaseSearchResults.ITEM_MAP.get(getArguments()
                    .getString(ARG_ITEM_ID));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_restaurant_detail,
                container, false);

        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.this_name)).setText(mItem
                    .getName());
            ((TextView) rootView.findViewById(R.id.this_cuisine)).setText(mItem
                    .getCuisine());
            ((TextView) rootView.findViewById(R.id.this_type)).setText(mItem
                    .getType());
            ((TextView) rootView.findViewById(R.id.this_ambiance))
                    .setText(mItem.getAmbiance());

            String rating = "";
            for (int i = 1; i <= mItem.getRating(); i++) {
                rating += "*";
            }
            ((TextView) rootView.findViewById(R.id.this_rating))
                    .setText(rating);

            String budget = "";
            for (int i = 1; i <= mItem.getBudget(); i++) {
                budget += "$";
            }
            ((TextView) rootView.findViewById(R.id.this_budget))
                    .setText(budget);

            ((TextView) rootView.findViewById(R.id.this_menu))
                    .setText(mItem.getMenu());
        }

        return rootView;
    }
}
