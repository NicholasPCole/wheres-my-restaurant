package edu.wit.bcd.wheresmyrestaurant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.os.Parcel;
import android.os.Parcelable;

public class DatabaseSearchResults implements Parcelable {
    private String status = null;
    private String message = null;
    private int matches = 0;
    private ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();
    public static Map<String, Restaurant> ITEM_MAP = new HashMap<String, Restaurant>();

    public static final Parcelable.Creator<DatabaseSearchResults> CREATOR = new Parcelable.Creator<DatabaseSearchResults>() {
        public DatabaseSearchResults createFromParcel(Parcel source) {
            return new DatabaseSearchResults(source);
        }

        public DatabaseSearchResults[] newArray(int size) {
            return new DatabaseSearchResults[size];
        }
    };

    public DatabaseSearchResults() {
        restaurants = new ArrayList<Restaurant>();
    }

    public DatabaseSearchResults(Parcel source) {
        this.status = source.readString();
        this.message = source.readString();
        this.matches = source.readInt();
        source.readTypedList(this.restaurants, Restaurant.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public int getMatches() {
        return matches;
    }

    public String getRestaurantNames() {
        String output = "";

        for (Restaurant restaurant : restaurants) {
            output += restaurant.toString() + "; ";
        }

        return output;
    }

    public ArrayList<Restaurant> getRestaurants() {
        return restaurants;
    }

    public void prepareMap() {
        ITEM_MAP.clear();

        for (Restaurant restaurant : restaurants) {
            ITEM_MAP.put(Integer.toString(restaurant.getId()), restaurant);
        }
    }

    @Override
    public String toString() {
        if (this.status.equals("OK")) {
            if (matches == 1) {
                return "There was 1 match.";
            } else {
                return "There were " + matches + " matches.";
            }
        } else if (this.status.equals("ERROR")) {
            return "There was an error (" + message + ").";
        } else {
            return "There was an unknown error.";
        }
    }

    public void writeToParcel(Parcel destination, int flags) {
        destination.writeString(status);
        destination.writeString(message);
        destination.writeInt(matches);
        destination.writeTypedList(restaurants);
    }

}
