package edu.wit.bcd.wheresmyrestaurant;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DatabaseSearchTask extends AsyncTask<String, Void, String> {
    private Context context;
    private String searchQuery = null;

    public DatabaseSearchTask(Context context) {
        this.context = context;
    }

    protected void onPreExecute() {

    }

    @Override
    protected String doInBackground(String... arg0) {
        try {
            // Retrieve the search query and define the MySQL server proxy.

            searchQuery = (String) arg0[0];

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(
                    "http://sdd.nicholaspcole.com/wmr.php");

            // Add the parameters to the POST request.

            List<NameValuePair> parameters = new ArrayList<NameValuePair>();
            parameters.add(new BasicNameValuePair("query", searchQuery));
            post.setEntity(new UrlEncodedFormEntity(parameters));

            // Send the POST request.

            HttpResponse response = client.execute(post);

            // Read the response.

            BufferedReader rd = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));
            String line;
            StringBuffer responseBuffer = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                responseBuffer.append(line);
            }

            return responseBuffer.toString();
        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(String jsonResponse) {
        Gson gson = new GsonBuilder().create();
        DatabaseSearchResults searchResults = gson
                .fromJson(jsonResponse, DatabaseSearchResults.class);

        Toast.makeText(context, searchResults.toString(), Toast.LENGTH_SHORT)
                .show();

        if (searchResults.getMatches() > 0) {
            // Only show a list of results if there are results to display.

            Intent listResultsIntent = new Intent(context,
                    RestaurantListActivity.class);
            listResultsIntent.putExtra("searchResults", searchResults);
            context.startActivity(listResultsIntent);
        }
    }
}