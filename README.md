Where’s My Restaurant?
======================

A project from Jason Brunelle, Nicholas Cole, and Fatou Diop.

There are two parent directories in the repository:

* `Where'sMyRestaurant?` is the Android application project folder for Eclipse.

* `db-web-api` contains a web API, written in PHP, serving as an interface between the Android application and MySQL database backend.
