<?php
    // The hostname or IP address of the MySQL server.
    define('DB_HOST', '127.0.0.1');

    // The name of the database.
    define('DB_NAME', '');

    // The MySQL user to connect with.
    define('DB_USER', '');

    // The password of the corresponding MySQL user.
    define('DB_PASS', '');
