<?php
    header('Content-type: application/json');

    if (!require_once('db-config.php')) {
        $output = array('status' => 'ERROR', 'message' => 'Configuration File Not Loaded');
        echo json_encode($output);
        exit(1);
    }

    // Check if a query is defined in the POST request.
    
    if (!isset($_POST['query'])) {
        $output = array('status' => 'ERROR', 'message' => 'Query Not Received');
        echo json_encode($output);
        exit(1);
    }

    // Connect to the database.

    $db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($db->connect_errno) {
        $output = array('status' => 'ERROR', 'message' => 'Database Connection Failure');
        echo json_encode($output);
        exit(1);
    }

    // Compose and execute the query.

    $ug_query = $_POST['query'];

    if (!$result = $db->query($ug_query)) {
        $output = array('status' => 'ERROR', 'message' => 'Database Query Failure (' . $db->error . ')');
        echo json_encode($output);
        exit(1);
    }

    // Parse and output the results.

    $matches = array();
    while ($row = $result->fetch_assoc()) {
        array_push($matches, $row);
    }

    $output = array('status' => 'OK', 'matches' => $result->num_rows, 'restaurants' => $matches);
    echo json_encode($output);

    // Close the result set and database connection.
    
    $result->close();
    $db->close();
?>
